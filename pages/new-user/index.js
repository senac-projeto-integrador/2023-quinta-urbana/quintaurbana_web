import React from "react";
import style from "./newUser.module.css";
import Header from "../../components/Header";
import InputDispensario from "../../components/InputDispesario";
import ButtonDisp from "../../components/ButtonDisp";
import Select from "../../components/Select";
import { mockSelectAdm, mockSelectFunction } from "../residents/utils/mocks";
import { useRouter } from "next/router";
import axios from "axios";

const NewResident = () => {
  const router = useRouter();
  const [user, setUser] = React.useState({
    nome: "",
    email: "",
    telefone: "",
    senha: "DIAXg23",
    cpf: "",
    idade: "",
    funcao: "",
    isAdmin: "",
  });

  const handleRegistertNewUser = async () => {
    if (
      !user.nome ||
      !user.email ||
      !user.telefone ||
      !user.cpf ||
      !user.idade ||
      !user.isAdmin ||
      !user.funcao
    ) {
      alert("Favor preencher todas as informações para cadastrar um usuário");
      return;
    }
    try {
      await axios.post("https://quinta-urbana.onrender.com/usuario/create", {
        nome: user.nome,
        email: user.email,
        telefone: user.telefone,
        senha: user.senha,
        cpf: user.cpf,
        idade: user.idade,
        funcao: user.funcao,
        isAdmin: user.isAdmin === "Sim" ? 1 : "0",
      });
      router.push("/users");
    } catch (err) {
      alert("Algo deu errado com esse serviço, favor tentar novamente");
      router.push("/home");
    }
  };

  return (
    <>
      <Header />
      <div className={style.boxContent}>
        <div>
          <div className={style.titulo}>
            <p>Novo Usuário</p>
          </div>
          <div className={style.dispStyle}>
            <div className={style.inputBox}>
              <InputDispensario
                label="Senha"
                placeholder="XXXXX"
                style={{ width: "100px" }}
                value={user.senha}
                onChange={({ target }) =>
                  setUser({ ...user, senha: target.value })
                }
                disabled
              />
              <InputDispensario
                label="Nome"
                placeholder="Nome do Usuário"
                value={user.nome}
                onChange={({ target }) =>
                  setUser({ ...user, nome: target.value })
                }
              />
            </div>
            <div className={style.inputBox}>
              <InputDispensario
                label="Email"
                placeholder="Email do Usuário"
                style={{ width: "150px" }}
                value={user.email}
                onChange={({ target }) =>
                  setUser({ ...user, email: target.value })
                }
              />
              <InputDispensario
                label="CPF"
                placeholder="111111111"
                style={{ width: "150px" }}
                value={user.cpf}
                onChange={({ target }) =>
                  setUser({ ...user, cpf: target.value })
                }
              />
            </div>
            <div className={style.inputBox}>
              <InputDispensario
                label="Idade"
                placeholder="28 anos"
                style={{ width: "150px" }}
                value={user.idade}
                onChange={({ target }) =>
                  setUser({ ...user, idade: target.value })
                }
              />
              <Select
                label="Função"
                selectArr={mockSelectFunction}
                style={{ width: "150px" }}
                value={user.funcao}
                onChange={({ target }) =>
                  setUser({ ...user, funcao: target.value })
                }
              />
            </div>
            <div className={style.inputBox}>
              <Select
                label="Usuário ADM?"
                selectArr={mockSelectAdm}
                style={{ width: "150px" }}
                value={user.isAdmin}
                onChange={({ target }) =>
                  setUser({ ...user, isAdmin: target.value })
                }
              />
              <InputDispensario
                label="Telefone"
                placeholder="(99) 9999-9999"
                style={{ width: "150px" }}
                value={user.telefone}
                onChange={({ target }) =>
                  setUser({ ...user, telefone: target.value })
                }
              />
            </div>
          </div>
        </div>

        <ButtonDisp title="Salvar" onClick={() => handleRegistertNewUser()} />
      </div>
    </>
  );
};

export default NewResident;
