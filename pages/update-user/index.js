import React from "react";
import style from "./updateUser.module.css";
import Header from "../../components/Header";
import InputDispensario from "../../components/InputDispesario";
import ButtonDisp from "../../components/ButtonDisp";
import Select from "../../components/Select";
import { mockSelectAdm, mockSelectFunction } from "../residents/utils/mocks";
import { useRouter } from "next/router";
import axios from "axios";

const UpdateResident = () => {
  const router = useRouter();
  const [user, setUser] = React.useState({
    nome: "",
    email: "",
    telefone: "",
    senha: "DIAXg23",
    cpf: "",
    idade: "",
    funcao: "",
    isAdmin: "",
  });
  const [data, setData] = React.useState("");

  React.useEffect(() => {
    const user = window.localStorage.getItem("user");
    const data = JSON.parse(user);
    setUser(JSON.parse(user));
    setData(data.nome);
  }, []);

  const handleUpdateUser = async () => {
    if (!user.nome || !user.telefone || !user.idade || !user.funcao) {
      alert("Favor preencher todas as informações para Atualizar um usuário");
      return;
    }
    try {
      await axios.put(
        `https://quinta-urbana.onrender.com/usuario/update/${data}`,
        {
          nome: user.nome,
          telefone: user.telefone,
          idade: user.idade,
          funcao: user.funcao,
        }
      );
      router.push("/users");
    } catch (err) {
      alert("Algo deu errado com esse serviço, favor tentar novamente");
      router.push("/home");
    }
  };

  return (
    <>
      <Header />
      <div className={style.boxContent}>
        <div>
          <div className={style.titulo}>
            <p>Novo Usuário</p>
          </div>
          <div className={style.dispStyle}>
            <div className={style.inputBox}>
              <InputDispensario
                label="Senha"
                placeholder="XXXXX"
                style={{ width: "100px" }}
                value={user.senha}
                onChange={({ target }) =>
                  setUser({ ...user, senha: target.value })
                }
                disabled
              />
              <InputDispensario
                label="Nome"
                placeholder="Nome do Usuário"
                value={user.nome}
                onChange={({ target }) =>
                  setUser({ ...user, nome: target.value })
                }
              />
            </div>
            <div className={style.inputBox}>
              <InputDispensario
                label="Email"
                placeholder="Email do Usuário"
                style={{ width: "150px" }}
                value={user.email}
                onChange={({ target }) =>
                  setUser({ ...user, email: target.value })
                }
                disabled
              />
              <InputDispensario
                label="CPF"
                placeholder="111111111"
                style={{ width: "150px" }}
                value={user.cpf}
                onChange={({ target }) =>
                  setUser({ ...user, cpf: target.value })
                }
                disabled
              />
            </div>
            <div className={style.inputBox}>
              <InputDispensario
                label="Idade"
                placeholder="28 anos"
                style={{ width: "150px" }}
                value={user.idade}
                onChange={({ target }) =>
                  setUser({ ...user, idade: target.value })
                }
              />
              <Select
                label="Função"
                selectArr={mockSelectFunction}
                style={{ width: "150px" }}
                value={user.funcao}
                onChange={({ target }) =>
                  setUser({ ...user, funcao: target.value })
                }
              />
            </div>
            <div className={style.inputBox}>
              <Select
                label="Usuário ADM?"
                selectArr={mockSelectAdm}
                style={{ width: "150px" }}
                value={user.isAdmin}
                onChange={({ target }) =>
                  setUser({ ...user, isAdmin: target.value })
                }
                disabled
              />
              <InputDispensario
                label="Telefone"
                placeholder="(99) 9999-9999"
                style={{ width: "150px" }}
                value={user.telefone}
                onChange={({ target }) =>
                  setUser({ ...user, telefone: target.value })
                }
              />
            </div>
          </div>
        </div>

        <ButtonDisp title="Salvar" onClick={() => handleUpdateUser()} />
      </div>
    </>
  );
};

export default UpdateResident;
