import React from "react";
import Header from "../../components/Header";
import InputDispensario from "../../components/InputDispesario";
import style from "./updateDispensario.module.css";
import ButtonDisp from "../../components/ButtonDisp";
import axios from "axios";
import { useRouter } from "next/router";
import { converterData, converterPreco } from "../dispensario/utils";

const UpdateDisp = () => {
  const router = useRouter();
  const [medicament, setMedicament] = React.useState({
    id: "",
    code: "",
    name: "",
    quant: "",
    dosage: "",
    price: "",
    validity: "",
  });
  React.useEffect(() => {
    const medicament = window.localStorage.getItem("medicament");
    const data = JSON.parse(medicament);
    setMedicament({
      id: data.id,
      code: data.codigo,
      name: data.nome,
      quant: data.quantidade,
      dosage: data.dosagem,
      price: data.preco.toLocaleString("pt-br", {
        currency: "BRL",
      }),
      validity: new Date(data?.validade).toLocaleDateString("pt-br"),
    });
  }, []);

  const handleUpdateMedicament = async () => {
    try {
      await axios.put(
        `https://quinta-urbana.onrender.com/med/update/cod/${medicament.code}`,
        {
          nome: medicament.name,
          dosagem: medicament.dosage,
          typo_dosagem: "mg",
          quantidade: medicament.quant,
          preco: converterPreco(medicament.price),
          foto: `https://${
            medicament.name + medicament.dosage + medicament.price
          }.jpg`,
          validade: converterData(medicament.validity),
        }
      );
      router.push("/dispensario");
    } catch (err) {
      alert("Algo deu errado com esse serviço, favor tentar novamente");
      router.push("/home");
    }
  };

  const handleDeleteMedicament = async () => {
    try {
      await axios.delete(
        `https://quinta-urbana.onrender.com/med/delete/${medicament.id}`
      );
      router.push("/dispensario");
    } catch (err) {
      alert("Algo deu errado com esse serviço, favor tentar novamente");
      router.push("/home");
    }
  };

  return (
    <>
      <Header />
      <div className={style.boxContent}>
        <div>
          <div
            className={style.titulo}
            onClick={() => setOpen((prev) => !prev)}
          >
            <p>Alteração de Medicamento</p>
          </div>
          <div className={style.dispStyle}>
            <div className={style.inputBox}>
              <InputDispensario
                label="Código"
                placeholder="Cód. Med."
                style={{ width: "100px" }}
                value={medicament.code}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, code: target.value })
                }
                disabled
              />
              <InputDispensario
                label="Nome / Sal"
                placeholder="Nome do Medicamento"
                value={medicament.name}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, name: target.value })
                }
              />
            </div>
            <div className={style.inputBox}>
              <InputDispensario
                label="Quant."
                placeholder="30 un"
                style={{ width: "70px" }}
                value={medicament.quant}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, quant: target.value })
                }
              />
              <InputDispensario
                label="Dosagem"
                placeholder="5 mg"
                style={{ width: "70px" }}
                value={medicament.dosage}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, dosage: target.value })
                }
              />
              <InputDispensario
                label="Valor Caixa"
                placeholder="R$ 47,90"
                style={{ width: "100px" }}
                value={medicament.price}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, price: target.value })
                }
              />
            </div>
            <div className={style.inputBox}>
              <InputDispensario
                label="Validade"
                placeholder="99/99/9999"
                style={{ width: "95px" }}
                value={medicament.validity}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, validity: target.value })
                }
              />
            </div>
          </div>
        </div>
        <ButtonDisp
          title="Desativar Medicamento"
          style={{
            backgroundColor: "#DA0000",
            marginBottom: "20px",
          }}
          onClick={handleDeleteMedicament}
        />
        <ButtonDisp title="Salvar" onClick={handleUpdateMedicament} />
      </div>
    </>
  );
};

export default UpdateDisp;
