import Image from "next/image";
import LogoArvore from "../../public/images/logo-arvore.svg";
import Input from "../../components/Input";
import Button from "../../components/Button";
import styles from "./login.module.css";
import { useRouter } from "next/router";

export const Login = () => {
  const router = useRouter();
  return (
    <div className={styles.boxOutPage}>
      <div className={styles.logoLoginPage}></div>
      <div className={styles.boxinputloginPage}>
        <div className={styles.boxCuidado}></div>
        <Input label="Usuário" />
        <Input label="Senha" type="password" />
        <a className={styles.linkStyle}>Esqueci a Senha</a>
        <div style={{ paddingBottom: "40px", paddingTop: "40px" }}>
          <Button text="Login" onClick={() => router.push("/home")} />
        </div>
        <Image
          src={LogoArvore}
          alt="Logo Arvore"
          className={styles.imageArvore}
        />
      </div>
    </div>
  );
};

export default Login;
