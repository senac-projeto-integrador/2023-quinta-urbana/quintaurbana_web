import React from "react";
import Header from "../../components/Header";
import styles from "./newResident.module.css";
import InputResidents from "../../components/InputResidents";
import Select from "../../components/Select";
import ButtonAnex from "../../components/ButtonAnex";
import Clipe from "../../public/images/clipe.png";
import { mockDep, mockExtact, mockStateCivil } from "./mock";
import { mockSelect } from "../residents/utils/mocks";
import InputDispensario from "../../components/InputDispesario";
import ButtonDisp from "../../components/ButtonDisp";
import axios from "axios";
import { useRouter } from "next/router";

const NewResident = () => {
  const router = useRouter();
  const [resident, setResident] = React.useState({
    nome: "",
    numero_registro: "",
    idade: 87,
    cep: "",
    sexo: "",
    estado_civil: "",
    estrato: "",
    plano: "",
    foto: "https://www.vigoalminuto.com/wp-content/uploads/2013/05/Parado-mayor-de-61-a%C3%B1os.jpg",
    grau_dependencia: "",
    ingresso: "",
    vacina_corona: "",
    vacina_gripe: "",
    alergia: "",
  });
  const [response, setResponse] = React.useState({
    nome: "",
    grauDeParentesco: "",
    contato: "",
  });
  const [medicament, setMedicament] = React.useState({
    descricao: "",
    qnt_dose: "",
    data_inicio: "",
    data_termino: "",
    horario_1: "",
    horario_2: "",
    horario_3: "",
    horario_4: "",
    horario_5: "",
    horario_6: "",
    medicamento_id: "",
  });

  const handleRegisterResident = async () => {
    if (
      !resident.nome ||
      !resident.numero_registro ||
      !resident.idade ||
      !resident.cep ||
      !resident.sexo ||
      !resident.estado_civil ||
      !resident.estrato ||
      !resident.plano ||
      !resident.grau_dependencia ||
      !resident.ingresso ||
      !resident.vacina_corona ||
      !resident.vacina_gripe ||
      !resident.alergia ||
      !response.nome ||
      !response.grauDeParentesco ||
      !response.contato ||
      !medicament.descricao ||
      !medicament.qnt_dose ||
      !medicament.data_inicio ||
      !medicament.data_termino ||
      !medicament.horario_1 ||
      !medicament.horario_2 ||
      !medicament.horario_3 ||
      !medicament.horario_4 ||
      !medicament.horario_5 ||
      !medicament.horario_6 ||
      !medicament.medicamento_id
    ) {
      alert("Necessário preencher todas informações para cadastrar um usuário");
      return;
    }
    try {
      axios.post(`https://quinta-urbana.onrender.com/residente/master`, {
        ...resident,
        ...response,
        ...medicament,
      });
      alert("Usuário cadastrado com Sucesso");
      router.push("/residents");
    } catch (err) {
      alert("Erro ao cadastrar usuário");
    }
  };

  return (
    <>
      <Header />
      <div className={styles.boxContent}>
        <p className={styles.titlePage}>Novo Resindente</p>
        <div className={styles.newResident}>
          <div className={styles.boxInput}>
            <InputResidents
              label="Código"
              placeholder="123456"
              style={{ width: "100px", marginRight: "20px" }}
              value={resident.numero_registro}
              onChange={({ target }) =>
                setResident({ ...resident, numero_registro: target.value })
              }
            />
            <InputResidents
              label="Nome"
              placeholder="Nome do Residente"
              style={{ width: "231px" }}
              value={resident.nome}
              onChange={({ target }) =>
                setResident({ ...resident, nome: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <InputResidents
              label="CEP"
              placeholder="20853081638"
              style={{ width: "119px", marginRight: "20px" }}
              value={resident.cep}
              onChange={({ target }) =>
                setResident({ ...resident, cep: target.value })
              }
            />
            <Select
              label="Grau Depen."
              selectArr={mockDep}
              style={{ width: "87px", marginRight: "20px" }}
              value={resident.grau_dependencia}
              onChange={({ target }) =>
                setResident({ ...resident, grau_dependencia: target.value })
              }
            />
            <Select
              label="Extrato"
              style={{ width: "67px" }}
              selectArr={mockExtact}
              value={resident.estrato}
              onChange={({ target }) =>
                setResident({ ...resident, estrato: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <Select
              label="Sexo"
              selectArr={mockSelect}
              style={{ width: "116px", marginRight: "20px" }}
              value={resident.sexo}
              onChange={({ target }) =>
                setResident({ ...resident, sexo: target.value })
              }
            />
            <InputResidents
              label="Plano de Saúde"
              placeholder="Nome do Convênio"
              style={{ width: "194px" }}
              value={resident.plano}
              onChange={({ target }) =>
                setResident({ ...resident, plano: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <Select
              label="Estado Civil"
              selectArr={mockStateCivil}
              style={{ width: "139px", marginRight: "20px" }}
              value={resident.estado_civil}
              onChange={({ target }) =>
                setResident({ ...resident, estado_civil: target.value })
              }
            />
            <InputResidents
              label="Ingresso."
              placeholder="99/99/9999"
              style={{ width: "105px" }}
              value={resident.ingresso}
              onChange={({ target }) =>
                setResident({ ...resident, ingresso: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <InputResidents
              label="Vacina Gripe"
              placeholder="99/99/9999"
              style={{ width: "105px", marginRight: "20px" }}
              value={resident.vacina_gripe}
              onChange={({ target }) =>
                setResident({ ...resident, vacina_gripe: target.value })
              }
            />
            <InputResidents
              label="Vacina Covid 19"
              placeholder="Nº de doses"
              style={{ width: "105px" }}
              value={resident.vacina_corona}
              onChange={({ target }) =>
                setResident({ ...resident, vacina_corona: target.value })
              }
            />
            <ButtonAnex
              title="Anexar"
              image={Clipe}
              style={{ width: "100px", marginLeft: "20px" }}
            />
          </div>
        </div>
        <div className={styles.newResident}>
          <p className={styles.titlePage}>Responsável</p>
          <div className={styles.boxInput}>
            <InputResidents
              label="Nome"
              placeholder="Nome do Responsável"
              style={{ width: "205px", marginRight: "20px" }}
              value={response.nome}
              onChange={({ target }) =>
                setResponse({ ...response, nome: target.value })
              }
            />
            <InputResidents
              label="Parentesco"
              placeholder="Parentesco"
              style={{ width: "105px" }}
              value={response.grauDeParentesco}
              onChange={({ target }) =>
                setResponse({ ...response, grauDeParentesco: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <InputResidents
              label="Contato"
              placeholder="(99) 99999-9999"
              style={{ width: "205px", marginRight: "20px" }}
              value={response.contato}
              onChange={({ target }) =>
                setResponse({ ...response, contato: target.value })
              }
            />
          </div>
        </div>
        <div className={styles.newResident}>
          <p className={styles.titlePage}>Alergias</p>
          <div>
            <div className={styles.alergicInput}>
              <InputResidents
                label="1º Alergia"
                placeholder="Descrição"
                style={{ width: "300px" }}
                value={resident.alergia}
                onChange={({ target }) =>
                  setResident({ ...resident, alergia: target.value })
                }
              />
            </div>
          </div>
        </div>

        <div className={styles.newResident}>
          <p className={styles.titlePage}>Prescição</p>

          <div>
            <p className={styles.titlePage}>Medicamento</p>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <InputDispensario
                label="Código"
                placeholder="Cód. Med."
                style={{ width: "100px" }}
                value={medicament.medicamento_id}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, medicamento_id: target.value })
                }
              />
              <InputDispensario
                label="Nome / Sal"
                placeholder="Nome do Medicamento"
                value={medicament.descricao}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, descricao: target.value })
                }
              />
            </div>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              {/* <InputDispensario
                label="Dosagem"
                placeholder="5 mg"
                style={{ width: "70px" }}
                value={medicament.qnt_dose}
                onChange={({ target }) =>
                  setResident({ ...resident, qnt_dose: target.value })
                }
              /> */}
              <InputDispensario
                label="Doses p/ dia"
                placeholder="4"
                style={{ width: "85px" }}
                value={medicament.qnt_dose}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, qnt_dose: target.value })
                }
              />
              <InputDispensario
                label="Data inicio"
                placeholder="15 dias"
                style={{ width: "77px" }}
                value={medicament.data_inicio}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, data_inicio: target.value })
                }
              />
              <InputDispensario
                label="Data fim"
                placeholder="15 dias"
                style={{ width: "77px" }}
                value={medicament.data_termino}
                onChange={({ target }) =>
                  setMedicament({ ...medicament, data_termino: target.value })
                }
              />
            </div>
            <p className={styles.text}>Horários</p>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div>
                <InputDispensario
                  placeholder="00:00"
                  style={{ width: "70px" }}
                  value={medicament.horario_1}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, horario_1: target.value })
                  }
                />
                <InputDispensario
                  placeholder="00:00"
                  style={{ width: "70px" }}
                  value={medicament.horario_2}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, horario_2: target.value })
                  }
                />
              </div>

              <div>
                <InputDispensario
                  placeholder="00:00"
                  style={{ width: "70px" }}
                  value={medicament.horario_3}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, horario_3: target.value })
                  }
                />
                <InputDispensario
                  placeholder="00:00"
                  style={{ width: "70px" }}
                  value={medicament.horario_4}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, horario_4: target.value })
                  }
                />
              </div>
              <div>
                <InputDispensario
                  placeholder="00:00"
                  style={{ width: "70px" }}
                  value={medicament.horario_5}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, horario_5: target.value })
                  }
                />
                <InputDispensario
                  placeholder="00:00"
                  style={{ width: "70px" }}
                  value={medicament.horario_6}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, horario_6: target.value })
                  }
                />
              </div>
            </div>
          </div>
        </div>
        <ButtonDisp
          title="Salvar"
          style={{ width: "330px", height: "29px", marginTop: "20px" }}
          onClick={() => handleRegisterResident()}
        />
      </div>
    </>
  );
};

export default NewResident;
