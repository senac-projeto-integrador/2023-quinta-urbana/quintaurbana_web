import React from "react";
import { Header } from "../../components/Header";
import ResidentCardMore from "../../components/ResidentCardMore";
import SelectInformation from "../../components/SelectInformation";
import Edit from "../../public/images/edit.png";
import Add from "../../public/images/Add.png";
import styles from "./resident.module.css";
import ButtonDisp from "../../components/ButtonDisp";
import OldMan from "../../public/images/Oldman.svg";
import axios from "axios";
import { useRouter } from "next/router";

const Resident = () => {
  const [resident, setResident] = React.useState("");
  const [data] = React.useState("");
  const router = useRouter();

  React.useEffect(() => {
    const resident = window.localStorage.getItem("resident");
    setResident(JSON.parse(resident));
  }, []);

  const handleDeleteResident = (id) => {
    try {
      axios.delete(`https://quinta-urbana.onrender.com/residente/delete/${id}`);
      alert("Residente Deletado");
      router.push("/residents");
    } catch (error) {
      alert("Algo deu errado com esse serviço, favor tentar novamente");
      router.push("/residents");
    }
  };

  return (
    <>
      <Header />
      <div>
        {resident && (
          <ResidentCardMore
            image={resident?.foto || OldMan}
            name={resident?.nome}
            age={resident?.idade}
            sex={resident?.sexo}
            dependency={resident?.grau_dependencia}
            responsible={resident?.Responsavel?.nome}
            family={resident?.Responsavel?.grauDeParentesco}
            id={resident.id}
            handleDeleteResident={handleDeleteResident}
          />
        )}
        <SelectInformation title="Informações Gerais" image={Edit}>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Ingresso:&nbsp;</p>
            <p className={styles.propsInformation}>
              {new Date(resident.ingresso).toLocaleDateString("pt-br")}
            </p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Plano de Saúde:&nbsp;</p>
            <p className={styles.propsInformation}>{resident.plano}</p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Vaciona Corona:&nbsp;</p>
            <p className={styles.propsInformation}>
              {!resident.vacina_corona
                ? "Não tomou"
                : new Date(resident.vacina_corona).toLocaleDateString("pt-br")}
            </p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Vaciona Gripe:&nbsp;</p>
            <p className={styles.propsInformation}>
              {!resident.vacina_gripe
                ? "Não tomou"
                : new Date(resident.vacina_gripe).toLocaleDateString("pt-br")}
            </p>
          </div>
        </SelectInformation>
        <SelectInformation title="Cuidados Diários" image={Edit}>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Medicamento:&nbsp;</p>
            <p className={styles.propsInformation}>
              {resident.Prescricao?.medicamento_id}
            </p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Quant. dose:&nbsp;</p>
            <p className={styles.propsInformation}>
              {resident.Prescricao?.qnt_dose}
            </p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Data de inicio:&nbsp;</p>
            <p className={styles.propsInformation}>
              {new Date(resident.Prescricao?.data_inicio).toLocaleDateString(
                "pt-br"
              )}
            </p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Data de termino:&nbsp;</p>
            <p className={styles.propsInformation}>
              {new Date(resident.Prescricao?.data_termino).toLocaleDateString(
                "pt-br"
              )}
            </p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Horários:&nbsp;</p>
            <div>
              <p className={styles.propsInformation}>
                {resident?.Prescricao?.horario_1}
              </p>
              <p className={styles.propsInformation}>
                {resident?.Prescricao?.horario_2}
              </p>
              <p className={styles.propsInformation}>
                {resident?.Prescricao?.horario_3}
              </p>
              <p className={styles.propsInformation}>
                {resident?.Prescricao?.horario_4}
              </p>
              <p className={styles.propsInformation}>
                {resident?.Prescricao?.horario_5}
              </p>
              <p className={styles.propsInformation}>
                {resident?.Prescricao?.horario_6}
              </p>
            </div>
          </div>
        </SelectInformation>
        <SelectInformation title="Histórico Clinico" image={Add}>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Histórico:&nbsp;</p>
            <p className={styles.propsInformation}>
              {resident.Prontuario?.historico || "Sem informação"}
            </p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Vacinas:&nbsp;</p>
            <p className={styles.propsInformation}>
              {resident.Prontuario?.vacinas || "Sem informação"}
            </p>
          </div>
        </SelectInformation>

        <SelectInformation title="Alergias" image={Add}>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Vacinas:&nbsp;</p>
            <p className={styles.propsInformation}>
              {resident.Alergium?.alergia}
            </p>
          </div>
        </SelectInformation>
        <SelectInformation title="Anotações" image={Add}>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Anotações</p>
          </div>
          <div style={{ display: "flex", justifyContent: "space-around" }}>
            <textarea></textarea>
            <ButtonDisp
              title="Enviar"
              style={{ width: "64px", height: "30px", padding: "7px" }}
            />
          </div>
        </SelectInformation>
      </div>
    </>
  );
};

export default Resident;
