import React from "react";
import { Header } from "../../components/Header";
import InputRange from "../../components/InputRange";
import InputResidents from "../../components/InputResidents";
import style from "./residents.module.css";
import Select from "../../components/Select";
import Button from "../../components/Button";
import ResidentCard from "../../components/ResidentCard";
import { mockResidents, mockSelect } from "./utils/mocks";
import { useRouter } from "next/router";

const Residents = () => {
  const router = useRouter();
  const handleSelectResident = (index) => {
    window.localStorage.setItem("resident", JSON.stringify(residents[index]));
    router.push("/resident");
  };
  const [residents, setResidents] = React.useState([]);

  const handleFetchResidents = async () => {
    const response = await fetch(
      "https://quinta-urbana.onrender.com/residente"
    );
    const data = await response.json();
    setResidents(data);
  };

  React.useEffect(() => {
    handleFetchResidents();
  }, []);
  return (
    <div>
      <Header />
      <div style={{ padding: "0px 30px" }}>
        <div className={style.informationBox}>
          <div className={style.inputBox}>
            <Select label="Sexo" selectArr={mockSelect} />
            <InputResidents label="Nome" placeholder="Nome do Residente" />
          </div>
          <div className={style.inputBoxButton}>
            <Button text="Filtrar" resi />
          </div>
        </div>
        <p className={style.label}>Lista de Residentes</p>
        <div>
          {residents.map((resident, i) => (
            <ResidentCard
              key={i}
              image={resident.foto}
              name={resident.nome}
              age={resident.idade}
              sex={resident.sexo}
              dependency={resident.grau_dependencia}
              responsible={resident.Responsavel.nome}
              family={resident.Responsavel.grauDeParentesco}
              onClick={() => handleSelectResident(i)}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Residents;
