export const mockResidents = [
  {
    name: "José Martins de Almeida",
    age: "65 anos",
    sex: "Masculino",
    dependency: "2",
    responsible: "Amélia Barbosa de Almeida",
    family: "Filha",
  },
  {
    name: "José Martins de Almeida",
    age: "65 anos",
    sex: "Masculino",
    dependency: "2",
    responsible: "Amélia Barbosa de Almeida",
    family: "Filha",
  },
  {
    name: "José Martins de Almeida",
    age: "65 anos",
    sex: "Masculino",
    dependency: "2",
    responsible: "Amélia Barbosa de Almeida",
    family: "Filha",
  },
  {
    name: "José Martins de Almeida",
    age: "65 anos",
    sex: "Masculino",
    dependency: "2",
    responsible: "Amélia Barbosa de Almeida",
    family: "Filha",
  },
  {
    name: "José Martins de Almeida",
    age: "65 anos",
    sex: "Masculino",
    dependency: "2",
    responsible: "Amélia Barbosa de Almeida",
    family: "Filha",
  },
];

export const mockSelect = [
  {
    name: "M",
  },
  {
    name: "F",
  },
];
export const mockSelectAdm = [
  {
    name: "",
  },
  {
    name: "Sim",
  },
  {
    name: "Não",
  },
];
export const mockSelectFunction = [
  {
    name: "",
  },
  {
    name: "ENFERMEIRO",
  },
  {
    name: "LIMPEZA",
  },
  {
    name: "COZINHEIRO",
  },
  {
    name: "MEDICO",
  },
  {
    name: "TERAPEUTA",
  },
  {
    name: "ADMINISTRADOR",
  },
  {
    name: "GERENTE",
  },
  {
    name: "SEGURANÇA",
  },
  {
    name: "NUTRICIONISTA",
  },
  {
    name: "FISIOTERAPEUTA",
  },
];
