import React from "react";
import styles from "./updateResident.module.css";
import ButtonAnex from "../../components/ButtonAnex";
import InputResidents from "../../components/InputResidents";
import Select from "../../components/Select";
import Header from "../../components/Header";
import { useRouter } from "next/router";
import { mockDep, mockExtact, mockStateCivil } from "../new-resident/mock";
import { mockSelect } from "../residents/utils/mocks";
import axios from "axios";
import ButtonDisp from "../../components/ButtonDisp";

const UpdateResident = () => {
  const router = useRouter();
  const [id, setId] = React.useState("");
  const [resident, setResident] = React.useState({
    nome: "",
    numero_registro: "",
    idade: "",
    cep: "",
    sexo: "",
    estado_civil: "",
    estrato: "",
    plano: "",
    foto: "https://www.vigoalminuto.com/wp-content/uploads/2013/05/Parado-mayor-de-61-a%C3%B1os.jpg",
    grau_dependencia: "",
    ingresso: "",
    vacina_corona: "",
    vacina_gripe: "",
    alergia: "",
  });

  React.useEffect(() => {
    const residentGet = window.localStorage.getItem("resident");
    const residentJson = JSON.parse(residentGet);
    setId(residentJson.id);
    setResident({
      nome: residentJson.nome,
      numero_registro: residentJson.numero_registro,
      idade: residentJson.idade,
      cep: residentJson.cep,
      sexo: residentJson.sexo,
      estado_civil: residentJson.estado_civil,
      estrato: residentJson.estrato,
      plano: residentJson.plano,
      foto: "https://www.vigoalminuto.com/wp-content/uploads/2013/05/Parado-mayor-de-61-a%C3%B1os.jpg",
      grau_dependencia: residentJson.grau_dependencia,
      ingresso: residentJson.ingresso,
      vacina_corona: residentJson.vacina_corona,
      vacina_gripe: residentJson.vacina_gripe,
      alergia: residentJson.Alergium?.alergia,
    });
  }, []);

  const handleUpdateResident = () => {
    if (
      !resident.nome ||
      !resident.numero_registro ||
      !resident.idade ||
      !resident.cep ||
      !resident.sexo ||
      !resident.estado_civil ||
      !resident.estrato ||
      !resident.plano ||
      !resident.grau_dependencia ||
      !resident.ingresso
    ) {
      alert("Necessário preencher todas informações para atualizar um usuário");
      return;
    }
    try {
      axios.put(`https://quinta-urbana.onrender.com/residente/update/${id}`, {
        ...resident,
      });
      router.push("/residents");
    } catch (err) {
      alert("Erro ao atualizar um usuário, tentar novamente mais tarde");
      router.push("/residents");
    }
  };
  console.log(resident);
  return (
    <>
      <Header />
      <div className={styles.boxContent}>
        <p className={styles.titlePage}>Alteração de cadastro</p>
        <div className={styles.newResident}>
          <div className={styles.boxInput}>
            <InputResidents
              label="Código"
              placeholder="123456"
              style={{ width: "100px", marginRight: "20px" }}
              value={resident.numero_registro}
              onChange={({ target }) =>
                setResident({ ...resident, numero_registro: target.value })
              }
            />
            <InputResidents
              label="Nome"
              placeholder="Nome do Residente"
              style={{ width: "231px" }}
              value={resident.nome}
              onChange={({ target }) =>
                setResident({ ...resident, nome: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <InputResidents
              label="CEP"
              placeholder="20853081638"
              style={{ width: "119px", marginRight: "20px" }}
              value={resident.cep}
              onChange={({ target }) =>
                setResident({ ...resident, cep: target.value })
              }
            />
            <Select
              label="Grau Depen."
              selectArr={mockDep}
              style={{ width: "87px", marginRight: "20px" }}
              value={resident.grau_dependencia}
              onChange={({ target }) =>
                setResident({ ...resident, grau_dependencia: target.value })
              }
            />
            <Select
              label="Extrato"
              style={{ width: "67px" }}
              selectArr={mockExtact}
              value={resident.estrato}
              onChange={({ target }) =>
                setResident({ ...resident, estrato: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <Select
              label="Sexo"
              selectArr={mockSelect}
              style={{ width: "116px", marginRight: "20px" }}
              value={resident.sexo}
              onChange={({ target }) =>
                setResident({ ...resident, sexo: target.value })
              }
            />
            <InputResidents
              label="Plano de Saúde"
              placeholder="Nome do Convênio"
              style={{ width: "194px" }}
              value={resident.plano}
              onChange={({ target }) =>
                setResident({ ...resident, plano: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <Select
              label="Estado Civil"
              selectArr={mockStateCivil}
              style={{ width: "139px", marginRight: "20px" }}
              value={resident.estado_civil}
              onChange={({ target }) =>
                setResident({ ...resident, estado_civil: target.value })
              }
            />
            <InputResidents
              label="Ingresso."
              placeholder="99/99/9999"
              style={{ width: "105px" }}
              value={resident.ingresso}
              onChange={({ target }) =>
                setResident({ ...resident, ingresso: target.value })
              }
            />
          </div>
          <div className={styles.boxInput}>
            <InputResidents
              label="Vacina Gripe"
              placeholder="99/99/9999"
              style={{ width: "105px", marginRight: "20px" }}
              value={resident.vacina_gripe}
              onChange={({ target }) =>
                setResident({ ...resident, vacina_gripe: target.value })
              }
            />
            <InputResidents
              label="Vacina Covid 19"
              placeholder="Nº de doses"
              style={{ width: "105px" }}
              value={resident.vacina_corona}
              onChange={({ target }) =>
                setResident({ ...resident, vacina_corona: target.value })
              }
            />
          </div>
        </div>
        <ButtonDisp
          title="Atualizar"
          style={{ width: "330px", height: "29px", marginTop: "20px" }}
          onClick={() => handleUpdateResident()}
        />
      </div>
    </>
  );
};

export default UpdateResident;
