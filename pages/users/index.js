import React from "react";
import style from "./users.module.css";
import { Header } from "../../components/Header";
import { useRouter } from "next/router";
import InputResidents from "../../components/InputResidents";
import Select from "../../components/Select";
import { mockSelect, mockSelectFunction } from "../residents/utils/mocks";
import ButtonDisp from "../../components/ButtonDisp";
import UserCard from "../../components/UserCard";
import ButtonAnex from "../../components/ButtonAnex";

const Users = () => {
  const router = useRouter();
  const [users, setUsers] = React.useState([]);
  const [filterValue, setFilterValue] = React.useState({
    name: "",
    func: "",
  });
  const [buttonFilter, setButtonFilter] = React.useState(false);
  const [dadosFilter, setDadosFilter] = React.useState("");
  const handleSelectUser = (index) => {
    window.localStorage.setItem("user", JSON.stringify(users[index]));
    router.push("/user");
  };

  const handleFetchUsers = async () => {
    const response = await fetch("https://quinta-urbana.onrender.com/usuarios");
    const data = await response.json();
    setUsers(data);
  };

  React.useEffect(() => {
    handleFetchUsers();
  }, []);

  const handleFilterUser = () => {
    if (!filterValue.name && !filterValue.func) {
      alert("Insira alguma informação para filtrar");
      return;
    }
    if (filterValue.name && filterValue.func) {
      alert("O metodo de pesquisa é por Nome OU Função");
      return;
    }
    if (filterValue.name) {
      const data = users.filter((user) => user.nome.includes(filterValue.name));
      setDadosFilter(data);
      setButtonFilter(true);
    }
    if (filterValue.func) {
      const data = users.filter((user) =>
        user.funcao.includes(filterValue.func)
      );
      setDadosFilter(data);
      setButtonFilter(true);
    }
  };
  const handleClearFilter = () => {
    setFilterValue({
      name: "",
      func: "",
    });
    setDadosFilter(users);
    setButtonFilter(false);
  };
  return (
    <div>
      <Header />
      <div style={{ padding: "0px 30px" }}>
        <div className={style.informationBox}>
          <div className={style.inputBox}>
            <InputResidents
              label="Nome"
              placeholder="Nome do Usuário"
              style={{ width: "150px" }}
              value={filterValue.name}
              onChange={({ target }) =>
                setFilterValue({ ...filterValue, name: target.value })
              }
            />
            <Select
              label="Função"
              selectArr={mockSelectFunction}
              value={filterValue.func}
              onChange={({ target }) =>
                setFilterValue({ ...filterValue, func: target.value })
              }
            />
          </div>
          <div className={style.inputBoxSecond}>
            <ButtonDisp
              title="Filtrar"
              style={{
                width: "64px",
                height: "30px",
                padding: "7px",
              }}
              onClick={handleFilterUser}
            />
            {buttonFilter && (
              <ButtonAnex
                title="Limpar Filtro"
                style={{ width: "120px" }}
                onClick={() => handleClearFilter()}
              />
            )}
          </div>
        </div>
        <p className={style.label}>Lista de Usuários</p>
        <div>
          {users &&
            !dadosFilter &&
            users.map((user, i) => (
              <UserCard
                key={i}
                name={user.nome}
                age={user.idade + "anos"}
                code={user.codigo}
                func={user.funcao}
                onClick={() => handleSelectUser(i)}
              />
            ))}
          {users &&
            dadosFilter &&
            dadosFilter.map((user, i) => (
              <UserCard
                key={i}
                name={user.nome}
                age={user.idade + "anos"}
                code={user.codigo}
                func={user.funcao}
                onClick={() => handleSelectUser(i)}
              />
            ))}
        </div>
      </div>
    </div>
  );
};

export default Users;
