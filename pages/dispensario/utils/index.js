export const converterData = (data) => {
  let partes = data.split("/");
  let dia = partes[0];
  let mes = partes[1];
  let ano = partes[2];

  const novaData = `${ano}/${mes}/${dia}`;

  return novaData;
};

export const converterPreco = (preco) => {
  const novoPreco = preco.replace(/\./g, "").replace(",", ".");

  return novoPreco;
};
