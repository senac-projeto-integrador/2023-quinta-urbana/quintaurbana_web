import React from "react";
import { Header } from "../../components/Header";
import style from "./dispensario.module.css";
import Image from "next/image";
import buttonDisp from "../../public/images/buttonDispensario.svg";
import InputDispensario from "../../components/InputDispesario";
import ButtonAnex from "../../components/ButtonAnex";
import ButtonDisp from "../../components/ButtonDisp";
import Left from "../../public/images/left.png";
import DispensarioCard from "../../components/DispensarioCard";
import axios from "axios";
import { useRouter } from "next/router";
import { converterData, converterPreco } from "./utils";

const Dispensario = () => {
  const router = useRouter();
  const [open, setOpen] = React.useState(false);
  const [buttonFilter, setButtonFilter] = React.useState(false);
  const [medicaments, setMedicaments] = React.useState([]);
  const [dadosFilter, setDadosFilter] = React.useState("");
  const [medicament, setMedicament] = React.useState({
    name: "",
    quant: "",
    dosage: "",
    price: "",
    validity: "",
  });
  const [filterMedicament, setFilterMedicament] = React.useState({
    code: "",
    name: "",
  });

  const handleFetchMedicaments = async () => {
    const response = await fetch("https://quinta-urbana.onrender.com/med");
    const data = await response.json();
    setMedicaments(data);
  };

  const handleRegistertMedicament = async () => {
    if (!medicament.name || !medicament.dosage || !medicament.price) {
      alert("Erro ao cadastrar o medicamento");
      return;
    }
    try {
      await axios.post("https://quinta-urbana.onrender.com/med/create", {
        nome: medicament.name,
        dosagem: medicament.dosage,
        typo_dosagem: "mg",
        quantidade: medicament.quant,
        preco: converterPreco(medicament.price),
        foto: `https://${
          medicament.name + medicament.dosage + medicament.price
        }.jpg`,
        validade: converterData(medicament.validity),
      });
      setMedicament({
        name: "",
        quant: "",
        dosage: "",
        price: "",
        validity: "",
      });

      window.location.reload();
    } catch (err) {
      alert("Algo deu errado com esse serviço, favor tentar novamente");
      router.push("/home");
    }
  };

  const handleFilterMedicament = () => {
    if (!filterMedicament.name && !filterMedicament.code) {
      alert("Insira alguma informação para filtrar");
      return;
    }
    if (filterMedicament.name && filterMedicament.code) {
      alert("O metodo de pesquisa é por Nome OU Código");
      return;
    }
    const data = medicaments.filter((medicament) =>
      medicament.nome.includes(filterMedicament.name)
    );
    setDadosFilter(data);
    setButtonFilter(true);
  };

  const handleClearFilter = () => {
    setFilterMedicament({
      code: "",
      name: "",
    });
    setDadosFilter(medicaments);
    setButtonFilter(false);
  };

  const handleSelectMedicamentNoFilter = (i) => {
    window.localStorage.setItem("medicament", JSON.stringify(medicaments[i]));
    router.push("/update-dispensario");
  };

  const handleSelectMedicamentFilter = (i) => {
    window.localStorage.setItem("medicament", JSON.stringify(dadosFilter[i]));
    router.push("/update-dispensario");
  };
  React.useEffect(() => {
    handleFetchMedicaments();
  }, []);

  return (
    <>
      <Header />
      <div className={style.boxContent}>
        <div>
          <div
            className={style.titulo}
            onClick={() => setOpen((prev) => !prev)}
          >
            <Image src={open ? buttonDisp : Left} alt="ButtonDispario" />
            <p>Cadastro de Medicamento</p>
          </div>
          {open && (
            <div className={style.dispStyle}>
              <div className={style.inputBox}>
                <InputDispensario
                  label="Nome / Sal"
                  placeholder="Nome do Medicamento"
                  value={medicament.name}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, name: target.value })
                  }
                />
              </div>
              <div className={style.inputBox}>
                <InputDispensario
                  label="Quant."
                  placeholder="30 un"
                  style={{ width: "70px" }}
                  value={medicament.quant}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, quant: target.value })
                  }
                />
                <InputDispensario
                  label="Dosagem"
                  placeholder="5 mg"
                  style={{ width: "70px" }}
                  value={medicament.dosage}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, dosage: target.value })
                  }
                />
                <InputDispensario
                  label="Valor Caixa"
                  placeholder="R$ 47,90"
                  style={{ width: "100px" }}
                  value={medicament.price}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, price: target.value })
                  }
                />
              </div>
              <div className={style.inputBox}>
                <InputDispensario
                  label="Validade"
                  placeholder="99/99/9999"
                  style={{ width: "95px" }}
                  value={medicament.validity}
                  onChange={({ target }) =>
                    setMedicament({ ...medicament, validity: target.value })
                  }
                />

                <ButtonDisp
                  title="Cadastrar"
                  style={{ width: "100px", height: "30px" }}
                  onClick={() => handleRegistertMedicament()}
                />
              </div>
            </div>
          )}
        </div>
        <div className={style.inputBox}>
          <InputDispensario
            label="Nome / Sal"
            placeholder="Nome do Medicamento"
            value={filterMedicament.name}
            onChange={({ target }) =>
              setFilterMedicament({ ...filterMedicament, name: target.value })
            }
          />
          <InputDispensario
            label="Código"
            placeholder="Cód. Med."
            style={{ width: "100px" }}
            value={filterMedicament.code}
            onChange={({ target }) =>
              setFilterMedicament({ ...filterMedicament, code: target.value })
            }
          />
        </div>
        <div className={style.inputBox}>
          <ButtonDisp
            title="Filtrar"
            style={{ width: "64px", height: "30px", padding: "7px" }}
            onClick={() => handleFilterMedicament()}
          />
          {buttonFilter && (
            <ButtonAnex
              title="Limpar Filtro"
              style={{ width: "120px" }}
              onClick={() => handleClearFilter()}
            />
          )}
        </div>
        <p className={style.titlePage}>Medicamentos</p>
        <div>
          {medicaments &&
            !dadosFilter &&
            medicaments.map((medicament, i) => (
              <DispensarioCard
                key={i}
                code={medicament?.codigo}
                name={medicament?.nome}
                quant={medicament?.quantidade + " Un"}
                dosage={medicament?.dosagem + " " + medicament?.typo_dosagem}
                price={medicament?.preco?.toLocaleString("pt-br", {
                  style: "currency",
                  currency: "BRL",
                })}
                validity={new Date(medicament?.validade).toLocaleDateString(
                  "pt-br"
                )}
                onClick={() => handleSelectMedicamentNoFilter(i)}
              />
            ))}
          {medicaments &&
            dadosFilter &&
            dadosFilter.map((medicament, i) => (
              <DispensarioCard
                key={i}
                code={medicament?.codigo}
                name={medicament?.nome}
                quant={medicament?.quantidade + " Un"}
                dosage={medicament?.dosagem + " " + medicament?.typo_dosagem}
                price={medicament?.preco?.toLocaleString("pt-br", {
                  style: "currency",
                  currency: "BRL",
                })}
                validity={new Date(medicament?.validade).toLocaleDateString(
                  "pt-br"
                )}
                onClick={() => handleSelectMedicamentFilter(i)}
              />
            ))}
        </div>
      </div>
    </>
  );
};

export default Dispensario;
