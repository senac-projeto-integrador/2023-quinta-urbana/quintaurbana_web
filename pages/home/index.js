import React from "react";
import { Header } from "../../components/Header";
import ButtonWtIcon from "../../components/ButtonWtIcon";
import styles from "./home.module.css";
import { mockHomeButtons } from "./utils/mock";

export const Home = () => {
  return (
    <div>
      <Header />
      <div className={styles.boxButtons}>
        <div style={{ width: "100%" }}>
          {mockHomeButtons.map((button, i) => (
            <ButtonWtIcon
              title={button.title}
              image={button.image}
              all
              key={i}
              url={button.url}
            />
          ))}
        </div>
      </div>
    </div>
  );
};
export default Home;
