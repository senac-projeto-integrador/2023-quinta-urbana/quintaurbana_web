import Residentes from "../../../../public/images/users.svg";
import Dispensario from "../../../../public/images/inventory.svg";
import NewPerson from "../../../../public/images/person.svg";
import Person from "../../../../public/images/person2.png";

export const mockHomeButtons = [
  {
    title: "Residentes",
    image: Residentes,
    url: "/residents",
  },
  {
    title: "Dispensário",
    image: Dispensario,
    url: "/dispensario",
  },
  {
    title: "Usuários",
    image: Person,
    url: "/users",
  },
  {
    title: "Novo Residente",
    image: NewPerson,
    url: "/new-resident",
  },
  {
    title: "Novo Usuário",
    image: NewPerson,
    url: "/new-user",
  },
];
