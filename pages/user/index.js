import React, { use } from "react";
import { Header } from "../../components/Header";
import SelectInformation from "../../components/SelectInformation";
import Add from "../../public/images/filter_list.png";
import UserCardMore from "../../components/UserCardMore";
import ButtonDisp from "../../components/ButtonDisp";
import style from "./user.module.css";
import { useRouter } from "next/router";
import axios from "axios";

const Resident = () => {
  const router = useRouter();
  const [user, setUser] = React.useState("");
  const [data] = React.useState("");

  React.useEffect(() => {
    const user = window.localStorage.getItem("user");
    setUser(JSON.parse(user));
  }, []);

  function formatarNumero(numero) {
    const numeroString = numero.toString();
    const doisPrimeiros = numeroString.slice(0, 2);
    const outrosNumeros = numeroString.slice(2, -4);
    const quatroUltimos = numeroString.slice(-4);
    return `(${doisPrimeiros}) ${outrosNumeros}-${quatroUltimos}`;
  }

  const handleDeleteUser = async () => {
    try {
      await axios.delete(
        `https://quinta-urbana.onrender.com/usuario/delete/${user.id}`
      );
      alert("Usuário Deletado com Sucesso");
      router.push("/users");
    } catch (err) {
      alert("Algo deu errado com esse serviço, favor tentar novamente");
      router.push("/users");
    }
  };

  const handleUpdateAdm = async () => {
    try {
      await axios.put(
        `https://quinta-urbana.onrender.com/usuario/update/grant/params/${user.id}`
      );
      alert("Usuário atualizado");
      router.push("/users");
    } catch (err) {
      alert("Algo deu errado com esse serviço, favor tentar novamente");
      router.push("/users");
    }
  };

  return (
    <>
      <Header />
      <div>
        {user && (
          <UserCardMore
            image={user?.foto}
            name={user?.nome}
            age={user?.idade + " anos"}
            code={user?.codigo}
            func={user?.funcao}
            contact={formatarNumero(user?.telefone)}
          />
        )}
        <div className={style.boxButton}>
          <ButtonDisp
            title="Desativar Usuário"
            style={{
              width: "160px",
              height: "29px",
              backgroundColor: "#DA0000",
            }}
            onClick={handleDeleteUser}
          />
          {!user?.isAdmin ? (
            <ButtonDisp
              title="Tornar ADM"
              style={{
                width: "160px",
                height: "29px",
              }}
              onClick={() => handleUpdateAdm()}
            />
          ) : (
            <ButtonDisp
              title="Retirar ADM"
              style={{
                width: "160px",
                height: "29px",
                backgroundColor: "#F58634",
              }}
              onClick={() => handleUpdateAdm()}
            />
          )}
        </div>
        <SelectInformation
          title="Cuidados Realizados"
          image={Add}
        ></SelectInformation>
        <SelectInformation
          title="Anotações Realizadas"
          image={Add}
        ></SelectInformation>
      </div>
    </>
  );
};

export default Resident;
