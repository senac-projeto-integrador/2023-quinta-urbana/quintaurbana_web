/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      "jooinn.com",
      "localhost",
      "picsum.photos",
      "www.vigoalminuto.com",
    ], // <== Domain name
  },
  swcMinify: true,
};

module.exports = nextConfig;
