import react from "react"
import styles from './checkBox.module.css'

export const CheckBox = ({label}) => {
return (
<>
       <div className={styles.boxCheckBox}>
           <input type="checkbox" className={styles.checkBox}/>
           <p>{label}</p>
        </div>
</>
)
}

export default CheckBox