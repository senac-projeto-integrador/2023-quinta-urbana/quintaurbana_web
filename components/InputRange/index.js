import React from "react";
import style from "./inputRange.module.css";

const InputRange = ({ range, setRange }) => {
  return (
    <div>
      <p className={style.labelInput}>Idade</p>
      <input
        className={style.input}
        type="range"
        min={56}
        max={73}
        value={range}
        onChange={({ target }) => setRange(target.value)}
      />
      <div className={style.boxRange}>
        <p>56</p> <p>73</p>
      </div>
    </div>
  );
};

export default InputRange;
