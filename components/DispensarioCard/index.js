import Image from "next/image";
import React from "react";
import medicamentosDisp from "../../public/images/medicamentosDisp.svg";
import styles from "./DispensarioCard.module.css";

const DispensarioCard = ({
    code,
    name,
    quant,
    dosage,
    price,
    validity,
    ...props
}) => {
    return (
        <div className={styles.boxResident} {...props}>
            <Image src={medicamentosDisp} alt="Foto do Medicamento" />
            <div>
                <div className={styles.boxInformation}>
                    <p className={styles.labelInformation}>Código:&nbsp;</p>
                    <p className={styles.propsInformation}>{code}</p>
                </div>
                <div className={styles.boxInformationDoubleInformation}>
                    <div className={styles.boxInformation}>
                        <p className={styles.labelInformation}>Nome:&nbsp;</p>
                        <p className={styles.propsInformation}>{name}</p>
                    </div>
                </div>
                <div className={styles.boxInformationDoubleInformation}>
                    <div className={styles.boxInformation}>
                        <p className={styles.labelInformation}>
                            Quantidade:&nbsp;
                        </p>
                        <p className={styles.propsInformation}>{quant}</p>
                    </div>
                    <div className={styles.boxInformation}>
                        <p className={styles.labelInformation}>Dosagem:&nbsp;</p>
                        <p className={styles.propsInformation}>{dosage}</p>
                    </div>
                </div>
                <div className={styles.boxInformationDoubleInformation}>
                    <div className={styles.boxInformation}>
                        <p className={styles.labelInformation}>
                            Valor:&nbsp;
                        </p>
                        <p className={styles.propsInformation}>{price}</p>
                    </div>
                    <div className={styles.boxInformation}>
                        <p className={styles.labelInformation}>Validade:&nbsp;</p>
                        <p className={styles.propsInformation}>{validity}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DispensarioCard;
