import React from "react";
import style from "./buttonWtIcon.module.css";
import Image from "next/image";
import { useRouter } from "next/router";

const ButtonWtIcon = ({ title, image, url, all }) => {
  const router = useRouter();
  return (
    <div
      className={!all ? style.boxButton : style.boxButtonAll}
      onClick={() => router.push(url)}
    >
      <Image alt="image-logo" src={image} />
      <p>{title}</p>
    </div>
  );
};

export default ButtonWtIcon;
