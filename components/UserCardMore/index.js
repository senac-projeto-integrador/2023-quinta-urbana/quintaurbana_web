import Image from "next/image";
import React from "react";
import Enf from "../../public/images/enf.png";
import Edit from "../../public/images/edit.png";
import styles from "./userCardMore.module.css";
import ButtonDisp from "../ButtonDisp";
import { useRouter } from "next/router";

const UserCardMore = ({ name, age, code, func, contact }) => {
  const router = useRouter();
  return (
    <div className={styles.boxContainer}>
      <div className={styles.firstDivisor}>
        <Image
          src={Enf}
          alt="Foto do Residente"
          width={200}
          height={150}
          style={{ borderRadius: "10px" }}
        />
        <p className={styles.propsInformationName}>{name}</p>
      </div>
      <div className={styles.boxSecondContainer}>
        <p className={styles.labelFirstInformation}>Dados Gerais</p>
        <Image
          src={Edit}
          alt="Edit"
          onClick={() => router.push("/update-user")}
        />
      </div>

      <div className={styles.boxResident}>
        <div>
          <div className={styles.boxInformationDoubleInformation}>
            <div className={styles.boxInformation}>
              <p className={styles.labelInformation}>Idade:&nbsp;</p>
              <p className={styles.propsInformation}>{age}</p>
            </div>
            <div className={styles.boxInformation}>
              <p className={styles.labelInformation}>Código:&nbsp;</p>
              <p className={styles.propsInformation}>{code}</p>
            </div>
          </div>

          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Função:&nbsp;</p>
            <p className={styles.propsInformation}>{func}</p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Contato:&nbsp;</p>
            <p className={styles.propsInformation}>{contact}</p>
          </div>
        </div>
      </div>
      <div className={styles.buttonArea}></div>
    </div>
  );
};

export default UserCardMore;
