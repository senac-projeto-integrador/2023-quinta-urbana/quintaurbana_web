import Image from "next/image";
import React from "react";
import OldMan from "../../public/images/Oldman.svg";
import styles from "./residentCard.module.css";

const ResidentCard = ({
  image,
  name,
  age,
  sex,
  dependency,
  responsible,
  family,
  ...props
}) => {
  return (
    <div className={styles.boxResident} {...props}>
      <Image
        src={image}
        alt="Foto do Residente"
        width={100}
        height={120}
        style={{ borderRadius: "10px" }}
      />
      <div style={{ paddingLeft: "10px" }}>
        <div className={styles.boxInformation}>
          <p className={styles.labelInformation}>Nome:&nbsp;</p>
          <p className={styles.propsInformation}>{name}</p>
        </div>
        <div className={styles.boxInformationDoubleInformation}>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Idade:&nbsp;</p>
            <p className={styles.propsInformation}>{age}</p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Sexo:&nbsp;</p>
            <p className={styles.propsInformation}>{sex}</p>
          </div>
        </div>
        <div className={styles.boxInformation}>
          <p className={styles.labelInformation}>Grau de Dependência:&nbsp;</p>
          <p className={styles.propsInformation}>{dependency}</p>
        </div>
        <div className={styles.boxInformation}>
          <p className={styles.labelInformation}>Resp:&nbsp;</p>
          <p className={styles.propsInformation}>{responsible}</p>
        </div>
        <div className={styles.boxInformation}>
          <p className={styles.labelInformation}>Grau Familiar:&nbsp;</p>
          <p className={styles.propsInformation}>{family}</p>
        </div>
      </div>
    </div>
  );
};

export default ResidentCard;
