import React from "react";
import styles from "./select.module.css";

const Select = ({ label, selectArr, ...props }) => {
  return (
    <div>
      <p className={styles.labelInput}>{label}</p>
      <select className={styles.selectStyle} {...props}>
        {selectArr.map((select, i) => (
          <option key={i} value={select.name}>
            {select.name}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;
