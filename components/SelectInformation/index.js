import Image from "next/image";
import React from "react";
import Down from "../../public/images/down.png";
import Left from "../../public/images/left.png";

import styles from "./selectInformation.module.css";

const SelectInformation = ({ title, image, children }) => {
  const [open, setOpen] = React.useState(false);
  return (
    <div>
      <div className={styles.boxContent}>
        <div
          onClick={() => setOpen((prev) => !prev)}
          className={styles.boxHeader}
        >
          <Image src={open ? Down : Left} alt="Arrow" />
          <div className={styles.boxSecond}>
            <p className={styles.titleHeader}>{title}</p>
            <Image src={image} alt="edit" className={styles.imageStyle} />
          </div>
        </div>
        {open && <div>{children}</div>}
      </div>
    </div>
  );
};

export default SelectInformation;
