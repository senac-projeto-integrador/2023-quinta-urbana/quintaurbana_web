import Image from "next/image";
import React from "react";
import Enf from "../../public/images/enf.png";
import styles from "./userCard.module.css";

const UserCard = ({ image, name, age, code, func, ...props }) => {
  return (
    <div className={styles.boxResident} {...props}>
      <Image
        src={Enf}
        alt="Foto do Residente"
        width={100}
        height={120}
        style={{ borderRadius: "10px" }}
      />
      <div style={{ paddingLeft: "10px" }}>
        <div className={styles.boxInformation}>
          <p className={styles.labelInformation}>Nome:&nbsp;</p>
          <p className={styles.propsInformation}>{name}</p>
        </div>
        <div className={styles.boxInformationDoubleInformation}>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Idade:&nbsp;</p>
            <p className={styles.propsInformation}>{age}</p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Codigo:&nbsp;</p>
            <p className={styles.propsInformation}>{code}</p>
          </div>
        </div>
        <div className={styles.boxInformation}>
          <p className={styles.labelInformation}>Função:&nbsp;</p>
          <p className={styles.propsInformation}>{func}</p>
        </div>
      </div>
    </div>
  );
};

export default UserCard;
