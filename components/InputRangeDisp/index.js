import React from "react";
import style from "./inputRangeDisp.module.css";

const InputRangeDisp = ({ range, setRange }) => {
  return (
    <div>
      <p className={style.labelInput}>Valor</p>
      <input
        className={style.input}
        type="range"
        min={0}
        max={200}
        value={range}
        onChange={({ target }) => setRange(target.value)}
      />
      <div className={style.boxRange}>
        <p>R$ 16,30</p> <p>R$ 147,90</p>
      </div>
    </div>
  );
};

export default InputRangeDisp;
