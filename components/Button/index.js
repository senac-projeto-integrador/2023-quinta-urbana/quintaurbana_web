import React from "react";
import styles from "./button.module.css";
export const Button = ({ text, resi, ...props }) => {
  return (
    <div className={styles.boxButtonStyle}>
      <button
        className={!resi ? styles.buttonLoginStyle : styles.buttonResidentStyle}
        {...props}
      >
        {text}
      </button>
    </div>
  );
};

export default Button;
