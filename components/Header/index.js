import React from "react";
import Image from "next/image";
import Logout from "../../public/images/logout.png";
import Cottage from "../../public/images/cottage.svg";
import BackButton from "../../public/images/arrow_back.png";
import styles from "./header.module.css";
import { useRouter } from "next/router";

export const Header = () => {
  const [modal, setModal] = React.useState(false);
  const router = useRouter();

  return (
    <div>
      <div className={styles.boxHeaderLayout}>
        {router.pathname !== "/home" && (
          <div onClick={() => router.back()}>
            <Image alt="back" src={BackButton} />
          </div>
        )}
        <p className={styles.headerText}>QUINTA URBANA</p>
        {router.pathname !== "/home" && (
          <div onClick={() => router.push("/home")}>
            <Image alt="back" src={Cottage} />
          </div>
        )}
        <div onClick={() => router.push("/")}>
          <Image src={Logout} alt="Menubar" width={30} />
        </div>
      </div>
    </div>
  );
};
export default Header;
