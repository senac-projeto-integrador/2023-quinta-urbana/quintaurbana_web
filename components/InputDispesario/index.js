import React from "react";
import styles from "./InputDispensario.module.css";

export const InputDispensario = ({ label, ...props }) => {
  return (
    <div className={styles.boxInputStyle}>
      <p className={styles.labelInput}>{label}</p>
      <input className={styles.inputStyle} {...props} />
    </div>
  );
};

export default InputDispensario;
