import React from "react";
import style from "./buttonAnex.module.css";
import Image from "next/image";

const ButtonAnex = ({ title, image, label, ...props }) => {
  return (
    <div>
      <p className={style.labelStyle}>{label}</p>
      <div className={style.buttonContainer} {...props}>
        {image && <Image alt="image-logo" src={image} />}
        <p>{title}</p>
      </div>
    </div>
  );
};

export default ButtonAnex;
