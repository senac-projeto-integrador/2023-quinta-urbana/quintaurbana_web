import React from "react";
import style from "./buttonDisp.module.css";
import Image from "next/image";

const ButtonDisp = ({ title, image, ...props }) => {
  return (
    <div className={style.buttonContainer} {...props}>
      <p>{title}</p>
    </div>
  );
};

export default ButtonDisp;
