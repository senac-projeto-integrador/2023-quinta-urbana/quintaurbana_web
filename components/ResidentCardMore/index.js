import Image from "next/image";
import React from "react";
import OldMan from "../../public/images/Oldman.svg";
import Edit from "../../public/images/edit.png";
import styles from "./residentCardMore.module.css";
import { useRouter } from "next/router";
import ButtonDisp from "../ButtonDisp";

const ResidentCardMore = ({
  image,
  name,
  age,
  sex,
  dependency,
  responsible,
  family,
  id,
  handleDeleteResident,
}) => {
  const router = useRouter();
  return (
    <div className={styles.boxContainer}>
      <div className={styles.firstDivisor}>
        <Image
          src={image || OldMan}
          alt="Foto do Residente"
          width={200}
          height={150}
          style={{ borderRadius: "10px" }}
        />
        <p className={styles.propsInformationName}>{name}</p>
      </div>
      <div className={styles.boxSecondContainer}>
        <p className={styles.labelFirstInformation}>Dados Gerais</p>
        <Image
          src={Edit}
          alt="Edit"
          onClick={() => router.push("/update-resident")}
        />
      </div>

      <div className={styles.boxResident}>
        <div>
          <div className={styles.boxInformationDoubleInformation}>
            <div className={styles.boxInformation}>
              <p className={styles.labelInformation}>Idade:&nbsp;</p>
              <p className={styles.propsInformation}>{age}</p>
            </div>
            <div className={styles.boxInformation}>
              <p className={styles.labelInformation}>Sexo:&nbsp;</p>
              <p className={styles.propsInformation}>{sex}</p>
            </div>
          </div>
          <div className={styles.boxInformationDoubleInformation}>
            <div className={styles.boxInformation}>
              <p className={styles.labelInformation}>
                Grau de Dependência:&nbsp;
              </p>
              <p className={styles.propsInformation}>{dependency}</p>
            </div>
            <div className={styles.boxInformation}>
              <p className={styles.labelInformation}>Estrato:&nbsp;</p>
              <p className={styles.propsInformation}>4</p>
            </div>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Resp:&nbsp;</p>
            <p className={styles.propsInformation}>{responsible}</p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Grau Familiar:&nbsp;</p>
            <p className={styles.propsInformation}>{family}</p>
          </div>
          <div className={styles.boxInformation}>
            <p className={styles.labelInformation}>Contato:&nbsp;</p>
            <p className={styles.propsInformation}>(99) 99999-9999</p>
          </div>
        </div>
      </div>
      <ButtonDisp
        title="Desativar Residente"
        style={{ backgroundColor: "#DA0000" }}
        onClick={() => handleDeleteResident(id)}
      />
    </div>
  );
};

export default ResidentCardMore;
