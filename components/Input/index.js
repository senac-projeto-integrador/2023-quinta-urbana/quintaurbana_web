import React from "react";
import styles from "./input.module.css";

export const Input = ({ label, ...props }) => {
  return (
    <div className={styles.boxInputStyle}>
      <input className={styles.inputStyle} placeholder={label} {...props} />
    </div>
  );
};

export default Input;
